# Foundation HTML, CSS, and JavaScript

## What is this?

A *very* basic introduction into Hypertext Markup Language (HTML), Cascading Style Sheets (CSS), and JavaScript (JS).

## What does it cover?

We'll cover choosing and installing the right tools for the job, and then how to get some basic action happening in the browser. The course will take the following structure:

1. HTML
2. CSS
3. CSS: Frameworks (Bootstrap)
4. CSS: Responsive Design
5. CSS: Zen Garden challenge
6. JavaScript
7. jQuery

## Who is it for?

Absolute beginners who want to learn how web pages are built and have a go themselves.

## Why have you written this?

It came up in conversation, so I put something together and figured I'd share it. I'll update/edit from time to time.

## Who the hell are you anyway?

I'm [Phil Sherry](https://philsherry.com/), and I've been a professional Front-End Developer since the '90s.

/*!
 * Resources, in no particular order:
 *
 * http://learnwebdev.net/
 * http://eloquentjavascript.net/
 * http://www.diveintojavascript.com/
 * https://addyosmani.com/resources/essentialjsdesignpatterns/book/
 * http://lab.victorcoulon.fr/javascript/vanilla-javascript-FTW.html
 * http://youmightnotneedjquery.com/
 */

(function ($) {

  // All our code is wrapped in an immediately-invokved function expression
  // (IIFE), which is a JavaScript function that runs as soon as it is defined.
  // This stops our code from loitering in the global namespace, and possibly
  // causing trouble with other scripts that are included in the page.
  //
  // Read: Immediately-Invoked Function Expression (IIFE)
  // http://benalman.com/news/2010/11/immediately-invoked-function-expression/

  'use strict';

  /*
   * Strict Mode is a new feature in ECMAScript 5 that allows you to place
   * a program, or a function, in a "strict" operating context. This strict
   * context prevents certain actions from being taken and throws more exceptions.
   *
   * Strict mode helps out in a couple ways:
   *  • It catches some common coding bloopers, throwing exceptions.
   *  • It prevents, or throws errors, when relatively "unsafe" actions are taken
   *     (such as gaining access to the global object).
   *  • It disables features that are confusing or poorly thought out.
   *
   * Read: ECMAScript 5 Strict Mode, JSON, and More
   * http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
   */

  // Read: Cutting the mustard
  // http://responsivenews.co.uk/post/18948466399/cutting-the-mustard

  if ('querySelector' in document && 'localStorage' in window && 'addEventListener' in window) {

    // The browser can get this far, so we'll add our functions here.

    // Add a .js class to <html> to aid JS-only CSS rules
    $('html').addClass('js');

    var FOUNDATIONHTMLCSSJS = function () {

      'use strict';

      // private variables/functions here

      return {

        // public variables/functions here

        yourFirstFunction: function () {
          // You shouldn't leave these in production code
          console.log("PiedPiper's algorithm has compressed your data. :)");
          console.info("PiedPiper has recently been acquired by Hooli.");
          console.warn("PiedPiper has been deprecated and replaced by Nucleus.");
          console.error("Something catastrophic has happened!");
        },

        buttonAddsContent: function () {
          $('#add-content').click(function (event) {
            $(this).text('Clicked!').attr('disabled', 'disabled').addClass('clicked');
            $('.jumbotron').append('<p>I was added via a jQuery function.</p>');
          });
        },

        buttonTogglesState: function () {
          $('#state-button').click(function (event) {
            $(this).toggleClass('is-showing');
            $('.totallyhidden').toggleClass('show');
          });
        }

      }; // return

    }();

  }

  $(function () {

    'use strict';

    FOUNDATIONHTMLCSSJS.yourFirstFunction();
    FOUNDATIONHTMLCSSJS.buttonAddsContent();
    FOUNDATIONHTMLCSSJS.buttonTogglesState();

  });

}(jQuery));
